import 'package:apetite_project/decimaPrimeiraPagina.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:apetite_project/recursos.dart';

class DecimaPagina extends StatefulWidget {
  @override
  _DecimaPagina createState() => _DecimaPagina();
}

class _DecimaPagina extends State<DecimaPagina> {
  static GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "10. EXPOSIÇÃO AO CONSUMO DO\n ALIMENTO PREPARADO ",
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold),
        ),
        elevation: 7.0,
        backgroundColor: Colors.red[900],
      ),
       floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButtonAnimator: FloatingActionButtonAnimator.scaling,
      floatingActionButton: FloatingActionButton(
        isExtended: true,
        backgroundColor: Colors.white,
        child: Icon(
          Icons.arrow_forward,
          color: Colors.redAccent,
        ),
        onPressed: () {
          if (_formKey.currentState.validate()) {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => DecimaPrimeiraPagina()));
          }
        },
      ),
      body: conteudo(),
    );
  }

  Widget conteudo() {
    return Builder(
      builder: (BuildContext context) => ListView(
        padding: EdgeInsets.only(left: 10.0, right: 10.0),
        children: <Widget>[
          Form(
            key: _formKey,
            child: Padding(
              padding: EdgeInsets.all(10),
              child: Column(
                children: <Widget>[
                   BuildPerguntas(id: 146),
                  BuildPerguntas(id: 147),
                  BuildPerguntas(id: 148),
                  BuildPerguntas(id: 149),
                  BuildPerguntas(id: 150),
                  BuildPerguntas(id: 151),
                  BuildPerguntas(id: 152),
                  BuildPerguntas(id: 153),
                  BuildPerguntas(id: 154),
                  BuildPerguntas(id: 155),
                  
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
  }
