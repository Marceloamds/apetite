import 'package:apetite_project/setimaPagina.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:apetite_project/recursos.dart';

class SextaPagina extends StatefulWidget {
  @override
  _SextaPagina createState() => _SextaPagina();
}

class _SextaPagina extends State<SextaPagina> {
  static GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "6. MANIPULADORES",
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
        ),
        elevation: 7.0,
        backgroundColor: Colors.red[900],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButtonAnimator: FloatingActionButtonAnimator.scaling,
      floatingActionButton: FloatingActionButton(
        isExtended: true,
        backgroundColor: Colors.white,
        child: Icon(
          Icons.arrow_forward,
          color: Colors.redAccent,
        ),
        onPressed: () {
          if (_formKey.currentState.validate()) {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => SetimaPagina()));
          }
        },
      ),
      body: conteudo(),
    );
  }
  Widget conteudo() {
    return Builder(
      builder: (BuildContext context) => ListView(
        padding: EdgeInsets.only(left: 10.0, right: 10.0),
        children: <Widget>[
          Form(
            key: _formKey,
            child: Padding(
              padding: EdgeInsets.all(10),
              child: Column(
                children: <Widget>[
                  BuildPerguntas(id: 90),
                  BuildPerguntas(id: 91),
                  BuildPerguntas(id: 92),
                  BuildPerguntas(id: 93),
                  BuildPerguntas(id: 94),
                  BuildPerguntas(id: 95),
                  BuildPerguntas(id: 96),
                  BuildPerguntas(id: 97),
                  BuildPerguntas(id: 98),
                  BuildPerguntas(id: 99),
                  BuildPerguntas(id: 100),
                  BuildPerguntas(id: 101),
                  BuildPerguntas(id: 102),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
