import 'package:apetite_project/quintaPagina.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:apetite_project/recursos.dart';

class QuartaPagina extends StatefulWidget {
  @override
  _QuartaPagina createState() => _QuartaPagina();
}

class _QuartaPagina extends State<QuartaPagina> {
  static GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "4. ABASTECIMENTO DE ÁGUA",
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
        ),
        elevation: 7.0,
        backgroundColor: Colors.red[900],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButtonAnimator: FloatingActionButtonAnimator.scaling,
      floatingActionButton: FloatingActionButton(
        isExtended: true,
        backgroundColor: Colors.white,
        child: Icon(
          Icons.arrow_forward,
          color: Colors.redAccent,
        ),
        onPressed: () {
          if (_formKey.currentState.validate()) {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => QuintaPagina()));
          }
        },
      ),
      body: conteudo(),
    );
  }

  Widget conteudo() {
    return Builder(
      builder: (BuildContext context) => ListView(
        padding: EdgeInsets.only(left: 10.0, right: 10.0),
        children: <Widget>[
          Form(
            key: _formKey,
            child: Padding(
              padding: EdgeInsets.all(10),
              child: Column(
                children: <Widget>[
                  BuildPerguntas(id: 78),
                  BuildPerguntas(id: 79),
                  BuildPerguntas(id: 80),
                  BuildPerguntas(id: 81),
                  BuildPerguntas(id: 82),
                  BuildPerguntas(id: 83),
                  BuildPerguntas(id: 84),
                  BuildPerguntas(id: 85),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
