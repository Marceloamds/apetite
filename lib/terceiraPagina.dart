import 'package:apetite_project/quartaPagina.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:apetite_project/recursos.dart';

class TerceiraPagina extends StatefulWidget {
  @override
  _TerceiraPagina createState() => _TerceiraPagina();
}

class _TerceiraPagina extends State<TerceiraPagina> {
  static GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "3. CONTROLE INTEGRADO DE\nVETORES E PRAGAS URBANAS",
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold),
        ),
        elevation: 7.0,
        backgroundColor: Colors.red[900],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButtonAnimator: FloatingActionButtonAnimator.scaling,
      floatingActionButton: FloatingActionButton(
        isExtended: true,
        backgroundColor: Colors.white,
        child: Icon(
          Icons.arrow_forward,
          color: Colors.redAccent,
        ),
        onPressed: () {
          if (_formKey.currentState.validate()) {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => QuartaPagina()));
          }
        },
      ),
      body: conteudo(),
    );
  }

  Widget conteudo() {
    return Builder(
      builder: (BuildContext context) => ListView(
        padding: EdgeInsets.only(left: 10.0, right: 10.0),
        children: <Widget>[
          Form(
            key: _formKey,
            child: Padding(
              padding: EdgeInsets.all(10),
              child: Column(
                children: <Widget>[
                  BuildPerguntas(id: 72),
                  BuildPerguntas(id: 73),
                  BuildPerguntas(id: 74),
                  BuildPerguntas(id: 75),
                  BuildPerguntas(id: 76),
                  BuildPerguntas(id: 77),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
