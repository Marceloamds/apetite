import 'package:flutter/material.dart';
import 'package:pdf/pdf.dart';
import 'package:printing/printing.dart';
import 'recursos.dart';
import 'package:pie_chart/pie_chart.dart';
import 'package:apetite_project/main.dart';
import 'package:apetite_project/Pdf.dart';
import 'dart:async';
import 'package:flutter/rendering.dart';


class TelaDeResultados extends StatefulWidget {
  @override
  _Resultados createState() => _Resultados();
}

int numGrupo = 0;

class _Resultados extends State<TelaDeResultados> {
  double contAdequado = 0;
  double contInadequado = 0;
  double contNaoSeAplica = 0;
  double soma = 0;
  Color color;
  //int numGrupo = 0;
  static String nomeEmp = getNomeEmp();
  Map<String, double> dataMap = Map();
  List<Color> colorList = [
    Colors.greenAccent,
    Colors.redAccent,
    Colors.blueAccent,
  ];

  @override
  void initState() {
    super.initState();
    for (int i = 0; i < 182; i++) {
      if (respAd[i]) {
        contAdequado++;
      } else if (respIna[i]) {
        contInadequado++;
      } else if (respNao[i]) {
        contNaoSeAplica++;
      }
    }
    soma = contInadequado + contAdequado;

    dataMap.putIfAbsent("Adequado", () => contAdequado);
    dataMap.putIfAbsent("Inadequado", () => contInadequado);
    dataMap.putIfAbsent("Não se aplica", () => contNaoSeAplica);

    contAdequado /= soma;
    contAdequado *= 100;

    if (contAdequado > 75)
      numGrupo = 1;
    else if (50 < contAdequado && contAdequado <= 75)
      numGrupo = 2;
    else
      numGrupo = 3;
  }

  @override
  Widget build(BuildContext context) {
    if (numGrupo == 1) {
      color = Colors.greenAccent;
    } else if (numGrupo == 2) {
      color = Colors.yellow[600];
    } else if (numGrupo == 3) {
      color = Colors.redAccent;
    }
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red[900],
        elevation: 7.0,
        centerTitle: true,
        title: Text(
          "Resultados",
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.share, color: Colors.white),
            onPressed: (){
              sharePdf();
            },
          ),
        ],
      ),
      /*floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButtonAnimator: FloatingActionButtonAnimator.scaling,
      floatingActionButton: FloatingActionButton(
        elevation: 5.0,
        mini: true,
        backgroundColor: Colors.white,
        child: Icon(Icons.share , color: Colors.black,),
        onPressed: (){
        sharePdf();
        },),*/
      backgroundColor: Colors.white,
      body: Stack(
        children: <Widget>[
          Image.asset(
            "images/fundoresultado.png",
            fit: BoxFit.cover,
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(10, 75, 25, 10),
                child: Text(
                  "Classificação de Boas Práticas da $nomeEmp",
                  style: TextStyle(
                    color: color,
                    fontSize: 25,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              Padding(
                padding: EdgeInsets.all(10.0),
                child: Card(
                  color: Colors.white,
                  elevation: 7,
                  child:  PieChart(
                  dataMap: dataMap, //Required parameter
                  legendFontColor: Colors.blueGrey[900],
                  legendFontSize: 14.0,
                  legendFontWeight: FontWeight.w500,
                  animationDuration: Duration(milliseconds: 800),
                  chartLegendSpacing: 32.0,
                  chartRadius: MediaQuery.of(context).size.width / 2.7,
                  showChartValuesInPercentage: true,
                  showChartValues: true,
                  showChartValuesOutside: true,
                  chartValuesColor: Colors.blueGrey[900].withOpacity(0.9),
                  colorList: colorList,
                  showLegends: true,
                  //initialAngle: math.pi*0.5,
                ), 
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(5, 25, 10, 15),
                child: Text(
                  "Grupo $numGrupo - ${contAdequado.toStringAsPrecision(3)}%\n de atendimento aos Itens",
                  style: TextStyle(
                    color: color,
                    fontSize: 25,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
                 Padding(
                          padding: EdgeInsets.only(top: 50.0, right: 20.0, left: 20 ),
                          child: Card(
                            color: Colors.transparent,
                            elevation: 10.0,
                              child: RaisedButton( 
                                color: Colors.white,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(50.0)),
                                onPressed: () {
                                  _printPdf();
                                },
                                child: Text("Finalizar Avaliação",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 23.0,
                                        fontWeight: FontWeight.bold)),
                               
                              ),
                          ),
                        ),
            ],
          )
        ],
      ),
    );
  }

  Future<void> _printPdf() async {
    print('Print ...');
    await Printing.layoutPdf(
        onLayout: (PdfPageFormat format) async =>
            (await generateDocument(format)).save());
  }
  Future sharePdf() async {
    PdfPageFormat format;
    print('click');
    await Printing.sharePdf(bytes: (await generateDocument(format)).save(), filename: 'Avaliação da $nomeEmp.pdf');
  }
}
