import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:apetite_project/recursos.dart';
import 'package:apetite_project/oitavaPagina.dart';

class SetimaPagina extends StatefulWidget {
  @override
  _SetimaPagina createState() => _SetimaPagina();
}

class _SetimaPagina extends State<SetimaPagina> {
  static GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "7. MATÉRIAS-PRIMAS,\nINGREDIENTES E EMBALAGENS",
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold),
        ),
        elevation: 7.0,
        backgroundColor: Colors.red[900],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButtonAnimator: FloatingActionButtonAnimator.scaling,
      floatingActionButton: FloatingActionButton(
        isExtended: true,
        backgroundColor: Colors.white,
        child: Icon(
          Icons.arrow_forward,
          color: Colors.redAccent,
        ),
        onPressed: () {
          if (_formKey.currentState.validate()) {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => OitavaPagina()));
          }
        },
      ),
      body: conteudo(),
    );
  }
  Widget conteudo() {
    return Builder(
      builder: (BuildContext context) => ListView(
        padding: EdgeInsets.only(left: 10.0, right: 10.0),
        children: <Widget>[
          Form(
            key: _formKey,
            child: Padding(
              padding: EdgeInsets.all(10),
              child: Column(
                children: <Widget>[
                  BuildPerguntas(id: 103),
                  BuildPerguntas(id: 104),
                  BuildPerguntas(id: 105),
                  BuildPerguntas(id: 106),
                  BuildPerguntas(id: 107),
                  BuildPerguntas(id: 108),
                  BuildPerguntas(id: 109),
                  BuildPerguntas(id: 110),
                  BuildPerguntas(id: 111),
                  BuildPerguntas(id: 112),
                  BuildPerguntas(id: 113),
                  BuildPerguntas(id: 114),
                  BuildPerguntas(id: 115),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
