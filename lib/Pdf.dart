//import 'package:flutter/material.dart';
import 'package:pdf/widgets.dart';
import 'package:pdf/pdf.dart';
import 'recursos.dart';
import 'main.dart';
import 'telaDeResultados&Print.dart';

Future<Document> generateDocument(PdfPageFormat format) async {
  final Document pdf =
      Document(title: 'Avaliação da ${getNomeEmp()}', author: '${getNome()}');
  pdf.addPage(MultiPage(
      crossAxisAlignment: CrossAxisAlignment.start,
      header: (Context context) {
        //Cabeçalho/título da Primeira página, quando existe mais de uma página
        if (context.pageNumber == 1) {
          return null;
        }
        return Container(
          alignment: Alignment.centerRight,
          decoration: const BoxDecoration(
              border: BoxBorder(
                  bottom: true, width: 0.5, color: PdfColors.grey)),
          child: Text('Avaliação de Boas Práticas da ${getNomeEmp()} feita por ${getNome()}',
              textAlign: TextAlign.center),
        );
      },
      footer: (Context context) {
        //Rodapé das páginas
        return Container(
            alignment: Alignment.centerRight,
            child: Text(
                'Página ${context.pageNumber} de ${context.pagesCount}',
                style: Theme.of(context)
                    .defaultTextStyle
                    .copyWith(color: PdfColors.grey)));
      },
      build: (Context context) => <Widget>[
            //Conteúdo do Pdf
            Header(
                //Cabeçalho das outras Páginas
                level: 0,
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text('Avalição de Boas Práticas da ${getNomeEmp()}',
                          textScaleFactor: 2,),
                    ])),
            Header(
              level: 2,
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      '1. EDIFICAÇÃO, INSTALAÇÕES, EQUIPAMENTOS, MÓVEIS E UTENSÍLIOS ',
                      style: TextStyle(
                      fontWeight: FontWeight.bold,
                      ),
                      /*textScaleFactor: 2*/
                    ),
                  ]),
            ),

           /* Paragraph(
                text: "Áreas Externas", textAlign: TextAlign.center),*/
            buildSubtitulosPdf(0),

            BuildPerguntasPdf(id: 0),
            BuildPerguntasPdf(id: 1),

           /* Paragraph(
                text: "Áreas Internas", textAlign: TextAlign.center),*/
            buildSubtitulosPdf(1),

            BuildPerguntasPdf(id: 2),
            BuildPerguntasPdf(id: 3),

            /*Paragraph(
                text: "Edificações e Instalações",
                textAlign: TextAlign.center),*/
            buildSubtitulosPdf(2),

            BuildPerguntasPdf(id: 4),
            BuildPerguntasPdf(id: 5),
            BuildPerguntasPdf(id: 6),
            BuildPerguntasPdf(id: 7),
            BuildPerguntasPdf(id: 8),
            BuildPerguntasPdf(id: 9),
            BuildPerguntasPdf(id: 10),
            BuildPerguntasPdf(id: 11),

            /*Paragraph(
                text: "Instalações Físicas - Pisos",
                textAlign: TextAlign.center),*/

            buildSubtitulosPdf(3),

            BuildPerguntasPdf(id: 12),
            BuildPerguntasPdf(id: 13),

           /* Paragraph(
                text: "Instalações Físicas - Paredes",
                textAlign: TextAlign.center),*/

             buildSubtitulosPdf(4),

            BuildPerguntasPdf(id: 14),
            BuildPerguntasPdf(id: 15),

            /*Paragraph(
                text: "Instalacoes Fisicas – Tetos/Forros",
                textAlign: TextAlign.center),*/
            buildSubtitulosPdf(5),

            //O problema esta na posição e/ou chamada 5 do método referente aos subtitulos

            BuildPerguntasPdf(id: 16),
            BuildPerguntasPdf(id: 17),

            //Pdf.Paragraph(text: "Portas", textAlign: Pdf.TextAlign.center),
            buildSubtitulosPdf(6),

            BuildPerguntasPdf(id: 18),
            BuildPerguntasPdf(id: 19),
            BuildPerguntasPdf(id: 20),

            /*Pdf.Paragraph(
                text: "Janelas e Outras Aberturas (sistema de exaustão)",
                textAlign: Pdf.TextAlign.center),*/
            buildSubtitulosPdf(7),

            BuildPerguntasPdf(id: 21),
            BuildPerguntasPdf(id: 22),
            BuildPerguntasPdf(id: 23),

            /*Pdf.Paragraph(
                text: "Ralos e Grelhas", textAlign: Pdf.TextAlign.center),*/
            buildSubtitulosPdf(8),

            BuildPerguntasPdf(id: 24),
            BuildPerguntasPdf(id: 25),

            /*Pdf.Paragraph(
                text: "Caixa de Gordura e Esgoto",
                textAlign: Pdf.TextAlign.center),*/
            buildSubtitulosPdf(9),

            BuildPerguntasPdf(id: 26),
            BuildPerguntasPdf(id: 27),
            BuildPerguntasPdf(id: 28),

            // Pdf.Paragraph(text: "Iluminação", textAlign: Pdf.TextAlign.center),
            buildSubtitulosPdf(10),

            BuildPerguntasPdf(id: 29),
            BuildPerguntasPdf(id: 30),

            //Pdf.Paragraph(text: "Ventilação", textAlign: Pdf.TextAlign.center),
            buildSubtitulosPdf(11),

            BuildPerguntasPdf(id: 31),
            BuildPerguntasPdf(id: 32),
            BuildPerguntasPdf(id: 33),

            //Pdf.Paragraph(
            //text: "Instalações Elétricas", textAlign: Pdf.TextAlign.center),

            buildSubtitulosPdf(12),
            BuildPerguntasPdf(id: 34),
            BuildPerguntasPdf(id: 35),

            /* Pdf.Paragraph(
                text: "Instalações Sanitárias",
                textAlign: Pdf.TextAlign.center),*/
            buildSubtitulosPdf(13),

            BuildPerguntasPdf(id: 36),
            BuildPerguntasPdf(id: 37),
            BuildPerguntasPdf(id: 38),
            BuildPerguntasPdf(id: 39),

            /*Pdf.Paragraph(
                text: "Lavatório Área de Manipulação",
                textAlign: Pdf.TextAlign.center),*/
            buildSubtitulosPdf(14),

            BuildPerguntasPdf(id: 40),
            BuildPerguntasPdf(id: 41),
            BuildPerguntasPdf(id: 42),

            /*Pdf.Paragraph(
                text: "Equipamentos", textAlign: Pdf.TextAlign.center),*/
            buildSubtitulosPdf(15),

            BuildPerguntasPdf(id: 43),
            BuildPerguntasPdf(id: 44),
            BuildPerguntasPdf(id: 45),
            BuildPerguntasPdf(id: 46),
            BuildPerguntasPdf(id: 47),
            //Pdf.Paragraph(text: "Utensilios", textAlign: Pdf.TextAlign.center),
            buildSubtitulosPdf(16),
            BuildPerguntasPdf(id: 48),
            BuildPerguntasPdf(id: 49),
            BuildPerguntasPdf(id: 50),
            //Pdf.Paragraph(text: "Moveis", textAlign: Pdf.TextAlign.center),
            buildSubtitulosPdf(17),
            BuildPerguntasPdf(id: 51),
            BuildPerguntasPdf(id: 52),
            BuildPerguntasPdf(id: 53),
            BuildPerguntasPdf(id: 54),

            Header(
              level: 2,
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      '2. HIGIENIZAÇÃO INSTALAÇÕES, EQUIPAMENTOS, MÓVEIS E UTENSÍLIOS',
                      style: TextStyle(
                      fontWeight: FontWeight.bold,
                      ),
                      /*textScaleFactor: 2*/
                    ),
                  ]),
            ),

            BuildPerguntasPdf(id: 55),

            BuildPerguntasPdf(id: 56),
            BuildPerguntasPdf(id: 57),
            BuildPerguntasPdf(id: 58),
            BuildPerguntasPdf(id: 59),
            BuildPerguntasPdf(id: 60),
            BuildPerguntasPdf(id: 61),
            BuildPerguntasPdf(id: 62),
            buildSubtitulosPdf(18),
            BuildPerguntasPdf(id: 63),
            BuildPerguntasPdf(id: 64),
            BuildPerguntasPdf(id: 65),
            BuildPerguntasPdf(id: 66),
            BuildPerguntasPdf(id: 67),
            buildSubtitulosPdf(19),
            BuildPerguntasPdf(id: 68),
            BuildPerguntasPdf(id: 69),
            BuildPerguntasPdf(id: 70),
            BuildPerguntasPdf(id: 71),
            
             Header(
              level: 2,
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      '3. CONTROLE INTEGRADO DE VETORES E PRAGAS URBANAS',
                      style: TextStyle(
                      fontWeight: FontWeight.bold,
                      ),
                      /*textScaleFactor: 2*/
                    ),
                  ]),
            ),

            BuildPerguntasPdf(id: 72),
            BuildPerguntasPdf(id: 73),
            BuildPerguntasPdf(id: 74),
            BuildPerguntasPdf(id: 75),
            BuildPerguntasPdf(id: 76),
            BuildPerguntasPdf(id: 77),

            Header(
              level: 2,
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      '4. ABASTECIMENTO DE ÁGUA',
                      style: TextStyle(
                      fontWeight: FontWeight.bold,
                      ),
                      /*textScaleFactor: 2*/
                    ),
                  ]),
            ),

            BuildPerguntasPdf(id: 78),
            BuildPerguntasPdf(id: 79),
            BuildPerguntasPdf(id: 80),
            BuildPerguntasPdf(id: 81),
            BuildPerguntasPdf(id: 82),
            BuildPerguntasPdf(id: 83),
            BuildPerguntasPdf(id: 84),
            BuildPerguntasPdf(id: 85),

            Header(
              level: 2,
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      '5. MANEJO DOS RESÍDUOS',
                      style: TextStyle(
                      fontWeight: FontWeight.bold,
                      ),
                      /*textScaleFactor: 2*/
                    ),
                  ]),
            ),

            BuildPerguntasPdf(id: 86),
            BuildPerguntasPdf(id: 87),
            BuildPerguntasPdf(id: 88),
            BuildPerguntasPdf(id: 89),

            Header(
              level: 2,
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      '6. MANIPULADORES',
                      style: TextStyle(
                      fontWeight: FontWeight.bold,
                      ),
                      /*textScaleFactor: 2*/
                    ),
                  ]),
            ),

            BuildPerguntasPdf(id: 90),
            BuildPerguntasPdf(id: 91),
            BuildPerguntasPdf(id: 92),
            BuildPerguntasPdf(id: 93),
            BuildPerguntasPdf(id: 94),
            BuildPerguntasPdf(id: 95),
            BuildPerguntasPdf(id: 96),
            BuildPerguntasPdf(id: 97),
            BuildPerguntasPdf(id: 98),
            BuildPerguntasPdf(id: 99),
            BuildPerguntasPdf(id: 100),
            BuildPerguntasPdf(id: 101),
            BuildPerguntasPdf(id: 102),

            Header(
              level: 2,
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      '7. MATÉRIAS-PRIMAS, INGREDIENTES E EMBALAGENS',
                      style: TextStyle(
                      fontWeight: FontWeight.bold,
                      ),
                      /*textScaleFactor: 2*/
                    ),
                  ]),
            ),

            BuildPerguntasPdf(id: 103),
            BuildPerguntasPdf(id: 104),
            BuildPerguntasPdf(id: 105),
            BuildPerguntasPdf(id: 106),
            BuildPerguntasPdf(id: 107),
            BuildPerguntasPdf(id: 108),
            BuildPerguntasPdf(id: 109),
            BuildPerguntasPdf(id: 110),
            BuildPerguntasPdf(id: 111),
            BuildPerguntasPdf(id: 112),
            BuildPerguntasPdf(id: 113),
            BuildPerguntasPdf(id: 114),
            BuildPerguntasPdf(id: 115),

            Header(
              level: 2,
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      '8. PREPARAÇÃO DO ALIMENTO',
                      style: TextStyle(
                      fontWeight: FontWeight.bold,
                      ),
                      /*textScaleFactor: 2*/
                    ),
                  ]),
            ),

            BuildPerguntasPdf(id: 116),
            BuildPerguntasPdf(id: 117),
            BuildPerguntasPdf(id: 118),
            BuildPerguntasPdf(id: 119),
            BuildPerguntasPdf(id: 120),
            BuildPerguntasPdf(id: 121),
            BuildPerguntasPdf(id: 122),

            BuildPerguntasPdf(id: 123),
            BuildPerguntasPdf(id: 124),
            BuildPerguntasPdf(id: 125),

            BuildPerguntasPdf(id: 126),
            BuildPerguntasPdf(id: 127),
            BuildPerguntasPdf(id: 128),

            BuildPerguntasPdf(id: 129),
            BuildPerguntasPdf(id: 130),
            BuildPerguntasPdf(id: 131),

            BuildPerguntasPdf(id: 132),
            BuildPerguntasPdf(id: 133),
            BuildPerguntasPdf(id: 134),

            BuildPerguntasPdf(id: 135),
            BuildPerguntasPdf(id: 136),
            BuildPerguntasPdf(id: 137),

            BuildPerguntasPdf(id: 138),
            BuildPerguntasPdf(id: 139),

             Header(
              level: 2,
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      '9. ARMAZENAMENTO E TRANSPORTE DO ALIMENTO PREPARADO',
                      style: TextStyle(
                      fontWeight: FontWeight.bold,
                      ),
                      /*textScaleFactor: 2*/
                    ),
                  ]),
            ),

            BuildPerguntasPdf(id: 140),

            BuildPerguntasPdf(id: 141),
            BuildPerguntasPdf(id: 142),
            BuildPerguntasPdf(id: 143),

            BuildPerguntasPdf(id: 144),
            BuildPerguntasPdf(id: 145),

            Header(
              level: 2,
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      '10. EXPOSIÇÃO AO CONSUMO DO ALIMENTO PREPARADO',
                      style: TextStyle(
                      fontWeight: FontWeight.bold,
                      ),
                      /*textScaleFactor: 2*/
                    ),
                  ]),
            ),

            BuildPerguntasPdf(id: 146),

            BuildPerguntasPdf(id: 147),
            BuildPerguntasPdf(id: 148),
            BuildPerguntasPdf(id: 149),

            BuildPerguntasPdf(id: 151),
            BuildPerguntasPdf(id: 152),
            BuildPerguntasPdf(id: 150),
            BuildPerguntasPdf(id: 153),
            BuildPerguntasPdf(id: 154),

             Header(
              level: 2,
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      '11. DOCUMENTAÇÃO E REGISTRO',
                      style: TextStyle(
                      fontWeight: FontWeight.bold,
                      ),
                      /*textScaleFactor: 2*/
                    ),
                  ]),
            ),

            BuildPerguntasPdf(id: 155),
            BuildPerguntasPdf(id: 156),
            BuildPerguntasPdf(id: 157),
            buildSubtitulosPdf(20),
            BuildPerguntasPdf(id: 158),
            BuildPerguntasPdf(id: 159),
            BuildPerguntasPdf(id: 160),
            BuildPerguntasPdf(id: 161),
            buildSubtitulosPdf(21),
            BuildPerguntasPdf(id: 162),
            BuildPerguntasPdf(id: 163),
            BuildPerguntasPdf(id: 164),
            BuildPerguntasPdf(id: 165),
            BuildPerguntasPdf(id: 166),
            buildSubtitulosPdf(22),
            BuildPerguntasPdf(id: 167),
            BuildPerguntasPdf(id: 168),
            BuildPerguntasPdf(id: 169),
            BuildPerguntasPdf(id: 170),
            BuildPerguntasPdf(id: 171),
            buildSubtitulosPdf(23),
            BuildPerguntasPdf(id: 172),
            BuildPerguntasPdf(id: 173),
            BuildPerguntasPdf(id: 174),
            BuildPerguntasPdf(id: 175),
            BuildPerguntasPdf(id: 176),
            BuildPerguntasPdf(id: 177),
            BuildPerguntasPdf(id: 178),
            BuildPerguntasPdf(id: 179),

            Header(
              level: 2,
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      '12. RESPONSABILIDADE',
                      style: TextStyle(
                      fontWeight: FontWeight.bold,
                      ),
                      /*textScaleFactor: 2*/
                    ),
                  ]),
            ),

            BuildPerguntasPdf(id: 180),
            BuildPerguntasPdf(id: 181),

            Header(
              level: 3,
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'O estabelecimento ${getNomeEmp()} se econtra no Grupo $numGrupo da\nClassificação de Boas Práticas em serviço de alimentação',
                      //textAlign: TextAlign.justify,
                      style: TextStyle(
                      fontWeight: FontWeight.bold,
                      ),
                      /*textScaleFactor: 2*/
                    ),
                  ]),
            ),

            /*Pdf.Paragraph(
              text:
                  "Avaliado por /*$_Conteudo.nome empresa avaliada - $_Conteudo.emp*/", //Motivo do problema na geração do pdf ?
            ),*/
            Padding(padding: const EdgeInsets.all(10)),
          ]));
  return pdf;
}

class BuildPerguntasPdf extends StatelessWidget {
  final int id;
  BuildPerguntasPdf({this.id});

  @override
  Widget build(Context context) {
    return Paragraph(
      text: perguntas[id] + " ${respostas[id]} \n ${ina[id]}",
      textAlign: TextAlign.left,
    );
  }
}

Widget buildPerguntasPdf(int id) {
  return Paragraph(
    text: perguntas[id] + " ${respostas[id]} \n ${ina[id]}",
    textAlign: TextAlign.left,
  );
}

Widget buildSubtitulosPdf(int id) {
  return //Paragraph(text: subtitulos[id], textAlign: TextAlign.center);
  Header(
              level: 3,
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      subtitulos[id],
                      style: TextStyle(
                      fontWeight: FontWeight.bold,
                      ),
                      /*textScaleFactor: 2*/
                    ),
                  ]),
   );
}
