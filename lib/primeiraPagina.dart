import 'package:apetite_project/segundaPagina.dart';
import 'package:apetite_project/telaDeResultados&Print.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:apetite_project/recursos.dart';
import 'package:apetite_project/main.dart';

class PrimeiraPagina extends StatefulWidget {
  @override
  _PrimeiraPagina createState() => _PrimeiraPagina();
}

class _PrimeiraPagina extends State<PrimeiraPagina> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return Conteudo();
  }
}

class Conteudo extends StatefulWidget {
  @override
  _Conteudo createState() => _Conteudo();
}

class _Conteudo extends State<Conteudo> {
  static String nome = getNome();
  static String emp = getNomeEmp();
  static GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    //SystemChrome.setEnabledSystemUIOverlays([]);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "1. EDIFICAÇÃO, INSTALAÇÕES,\nEQUIPAMENTOS, MÓVEIS E UTENSÍLIOS",
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold),
        ),
        elevation: 7.0,
        backgroundColor: Colors.red[900],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButtonAnimator: FloatingActionButtonAnimator.scaling,
      floatingActionButton: FloatingActionButton(
        isExtended: true,
        backgroundColor: Colors.white,
        child: Icon(
          Icons.arrow_forward,
          color: Colors.redAccent,
          /*color: colorSeta[0],*/
        ),
        onPressed: () {
          if (_formKey.currentState.validate()) {
            print("Botão apertado");
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => SegundaPagina()));
              /*Navigator.push(context,
                MaterialPageRoute(builder: (context) => TelaDeResultados()));*/
          }
        },
      ),
      body: Builder(
        builder: (BuildContext context) => ListView(
          padding: EdgeInsets.only(left: 10.0, right: 10.0),
          children: <Widget>[
            Form(
              key: _formKey,
              child: Padding(
                padding: EdgeInsets.all(10),
                child: Column(
                  children: <Widget>[
                    buildSubtitulos(0),
                    BuildPerguntas(id: 0),
                    BuildPerguntas(id: 1),
                    buildSubtitulos(1),
                    BuildPerguntas(id: 2),
                    BuildPerguntas(id: 3),
                    buildSubtitulos(2),
                    BuildPerguntas(id: 4),
                    BuildPerguntas(id: 5),
                    BuildPerguntas(id: 6),
                    BuildPerguntas(id: 7),
                    BuildPerguntas(id: 8),
                    BuildPerguntas(id: 9),
                    BuildPerguntas(id: 10),
                    BuildPerguntas(id: 11),
                    buildSubtitulos(3),
                    BuildPerguntas(id: 12),
                    BuildPerguntas(id: 13),
                    buildSubtitulos(4),
                    BuildPerguntas(id: 14),
                    BuildPerguntas(id: 15),
                    buildSubtitulos(5),
                    BuildPerguntas(id: 16),
                    BuildPerguntas(id: 17),
                    buildSubtitulos(6),
                    BuildPerguntas(id: 18),
                    BuildPerguntas(id: 19),
                    BuildPerguntas(id: 20),
                    buildSubtitulos(7),
                    BuildPerguntas(id: 21),
                    BuildPerguntas(id: 22),
                    BuildPerguntas(id: 23),
                    buildSubtitulos(8),
                    BuildPerguntas(id: 24),
                    BuildPerguntas(id: 25),
                    buildSubtitulos(9),
                    BuildPerguntas(id: 26),
                    BuildPerguntas(id: 27),
                    BuildPerguntas(id: 28),
                    buildSubtitulos(10),
                    BuildPerguntas(id: 29),
                    BuildPerguntas(id: 30),
                    buildSubtitulos(11),
                    BuildPerguntas(id: 31),
                    BuildPerguntas(id: 32),
                    BuildPerguntas(id: 33),
                    buildSubtitulos(12),
                    BuildPerguntas(id: 34),
                    BuildPerguntas(id: 35),
                    buildSubtitulos(13),
                    BuildPerguntas(id: 36),
                    BuildPerguntas(id: 37),
                    BuildPerguntas(id: 38),
                    BuildPerguntas(id: 39),
                    buildSubtitulos(14),
                    BuildPerguntas(id: 40),
                    BuildPerguntas(id: 41),
                    BuildPerguntas(id: 42),
                    buildSubtitulos(14),
                    BuildPerguntas(id: 43),
                    BuildPerguntas(id: 44),
                    BuildPerguntas(id: 45),
                    BuildPerguntas(id: 46),
                    BuildPerguntas(id: 47),
                    buildSubtitulos(15),
                    BuildPerguntas(id: 48),
                    BuildPerguntas(id: 49),
                    BuildPerguntas(id: 50),
                    buildSubtitulos(16),
                    BuildPerguntas(id: 51),
                    BuildPerguntas(id: 52),
                    BuildPerguntas(id: 53),
                    BuildPerguntas(id: 54),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
