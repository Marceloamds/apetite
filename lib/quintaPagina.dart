import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:apetite_project/recursos.dart';
import 'package:apetite_project/sextaPagina.dart';

class QuintaPagina extends StatefulWidget {
  @override
  _QuintaPagina createState() => _QuintaPagina();
}

class _QuintaPagina extends State<QuintaPagina> {
  static GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "5. MANEJO DOS RESÍDUOS",
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
        ),
        elevation: 7.0,
        backgroundColor: Colors.red[900],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButtonAnimator: FloatingActionButtonAnimator.scaling,
      floatingActionButton: FloatingActionButton(
        isExtended: true,
        backgroundColor: Colors.white,
        child: Icon(
          Icons.arrow_forward,
          color: Colors.redAccent,
        ),
        onPressed: () {
          if (_formKey.currentState.validate()) {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => SextaPagina()));
          }
        },
      ),
      body: conteudo(),
    );
  }

  Widget conteudo() {
    return Builder(
      builder: (BuildContext context) => ListView(
        padding: EdgeInsets.only(left: 10.0, right: 10.0),
        children: <Widget>[
          Form(
            key: _formKey,
            child: Padding(
              padding: EdgeInsets.all(10),
              child: Column(
                children: <Widget>[
                  BuildPerguntas(id: 86),
                  BuildPerguntas(id: 87),
                  BuildPerguntas(id: 88),
                  BuildPerguntas(id: 89),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
