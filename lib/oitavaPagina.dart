import 'package:apetite_project/NonaPagina.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:apetite_project/recursos.dart';

class OitavaPagina extends StatefulWidget {
  @override
  _OitavaPagina createState() => _OitavaPagina();
}

class _OitavaPagina extends State<OitavaPagina> {
  static GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "8. PREPARAÇÃO DO ALIMENTO",
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
        ),
        elevation: 7.0,
        backgroundColor: Colors.red[900],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButtonAnimator: FloatingActionButtonAnimator.scaling,
      floatingActionButton: FloatingActionButton(
        isExtended: true,
        backgroundColor: Colors.white,
        child: Icon(
          Icons.arrow_forward,
          color: Colors.redAccent,
        ),
        onPressed: () {
          if (_formKey.currentState.validate()) {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => NonaPagina()));
          }
        },
      ),
      body: conteudo(),
    );
  }

  Widget conteudo() {
    return Builder(
      builder: (BuildContext context) => ListView(
        padding: EdgeInsets.only(left: 10.0, right: 10.0),
        children: <Widget>[
          Form(
            key: _formKey,
            child: Padding(
              padding: EdgeInsets.all(10),
              child: Column(
                children: <Widget>[
                  BuildPerguntas(id: 117),
                  BuildPerguntas(id: 116),
                  BuildPerguntas(id: 118),
                  BuildPerguntas(id: 119),
                  BuildPerguntas(id: 120),
                  BuildPerguntas(id: 121),
                  BuildPerguntas(id: 122),
                  BuildPerguntas(id: 123),
                  BuildPerguntas(id: 124),
                  BuildPerguntas(id: 125),
                  BuildPerguntas(id: 126),
                  BuildPerguntas(id: 127),
                  BuildPerguntas(id: 128),
                  BuildPerguntas(id: 129),
                  BuildPerguntas(id: 130),
                  BuildPerguntas(id: 131),
                  BuildPerguntas(id: 32),
                  BuildPerguntas(id: 133),
                  BuildPerguntas(id: 134),
                  BuildPerguntas(id: 135),
                  BuildPerguntas(id: 136),
                  BuildPerguntas(id: 137),
                  BuildPerguntas(id: 138),
                  BuildPerguntas(id: 139),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
