import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

List<TextEditingController> inadequacoes = List<TextEditingController>();
List<bool> respAd =
    List<bool>(); //Guardará valores correspondendes a "Adequado"
List<bool> respNao =
    List<bool>(); //Guardará valores correspondendes a "não se aplica"
List<bool> respIna =
    List<bool>(); //Guardará valores correspondendes a "Inadequado"
List<String> respostas = List<String>();
List<String> perguntas = List<String>();
List<String> perguntasPdf = List<String>();
List<String> subtitulos = List<String>();
List<String> ina = List<String>();
List<bool> visible = List<bool>();
//List<Color> colorSeta = List<Color>();


class BuildPerguntas extends StatefulWidget {
  final int id;
  const BuildPerguntas({Key key, this.id}) : super(key: key);
  @override
  _BuildPerguntas createState() => _BuildPerguntas();
}

class _BuildPerguntas extends State<BuildPerguntas> {
  @override
  Widget build(BuildContext context) {
     return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Text(
            perguntas[widget.id],
            textAlign: TextAlign.left,
            style: TextStyle(fontSize: 20),
          ),
          CheckboxListTile(
            title: const Text("Adequado"),
            activeColor: Colors.greenAccent,
            onChanged: (bool value) {
              setState(
                () {
                  respAd[widget.id] = value;
                  if (value) {
                    respostas[widget.id] = "Adequado";
                    respNao[widget.id] = false;
                    respIna[widget.id] = false;
                    visible[widget.id] = false;
                    //colorSeta[widget.id] = Colors.black;
                  }
                },
              );
            },
            value: respAd[widget.id],
          ),
          CheckboxListTile(
            title: const Text("Inadequado"),
            activeColor: Colors.redAccent,
            onChanged: (bool value) {
              setState(
                () {
                  respIna[widget.id] = value;
                  if (value) {
                    respostas[widget.id] = "Inadequado";
                    respAd[widget.id] = false;
                    respNao[widget.id] = false;
                    visible[widget.id] = true;
                    //colorSeta[widget.id] = Colors.redAccent;
                  }
                },
              );
            },
            value: respIna[widget.id],
          ),
          CheckboxListTile(
            title: const Text(
              "Não se aplica",
            ),
            activeColor: Colors.blueAccent,
            onChanged: (bool value) {
              setState(
                () {
                  respNao[widget.id] = value;
                  if (value) {
                    respostas[widget.id] = "Não se aplica";
                    respAd[widget.id] = false;
                    respIna[widget.id] = false;
                    visible[widget.id] = false;
                    //colorSeta[widget.id] = Colors.black;
                  }
                },
              );
            },
            value: respNao[widget.id],
          ),
          Visibility(
            visible: visible[widget.id],
            child: Padding(
              padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
              child: TextFormField(
                autovalidate: false,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Descreva a inedequação',
                  helperText: "Síntese da discrição da inadequação",
                ),
                maxLines: 3,
                textInputAction: TextInputAction.done,
                textCapitalization: TextCapitalization.words,
                /*onEditingComplete: (){
                  colorSeta[widget.id] = Colors.black;
                },*/
                validator: (value) {
                  if (respIna[widget.id] && value.isEmpty)
                    return "É preciso descrever a inadequação";
                  else
                    ina[widget.id] = value;
                },
                controller: inadequacoes[widget.id],
              ),
            ),
          ),
          Divider(),
        ]);
  }
}


Widget buildSubtitulos(int id) {
  return Padding(
    padding: EdgeInsets.only(bottom: 5.0),
    child: Text(
      subtitulos[id],
      style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
      textAlign: TextAlign.center,
    ),
  );
}

void inicializarPerguntas() {
  int id = 0;
  perguntas.insert(
      id, "1.1. Estão livres de objetos em desuso ou estranhos ao ambiente?");
  id++;
  perguntas.insert(id, "1.2. Livre da presença de animais?");
  id++;
  perguntas.insert(
      id, "1.3. Estão livres de objetos em desuso ou estranhos ao ambiente?");
  id++;
  perguntas.insert(id, "1.4. Livre da presença de animais?");
  id++;
  perguntas.insert(id,
      "1.5. São projetadas de forma a possibilitar um fluxoordenado e sem cruzamentos?");
  id++;
  perguntas.insert(id,
      "1.6. São projetadas para facilitar as operações de manutenção, limpeza e, quando for o caso, desinfecção?");
  id++;
  perguntas.insert(id,
      "1.7. O acesso às instalações é independente e não comum a outros usos (ex. habitação)?");
  id++;
  perguntas.insert(id, "1.8. Existe controle do acesso de pessoal?");
  id++;
  perguntas.insert(
      id, "1.9. O dimensionamento é compatível com todas as operações?");
  id++;
  perguntas.insert(id,
      "1.10. Existe separação entre as diferentes atividades por meios físicos ou por outros meios eficazes?");
  id++;
  perguntas.insert(
      id, "1.11. As instalações são abastecidas de água corrente?");
  id++;
  perguntas.insert(id,
      "1.12. As instalações dispõem de conexões com rede de esgoto ou fossa séptica?");
  id++;
  perguntas.insert(
      id, "1.13. Possuem revestimentos lisos, impermeáveis elaváveis?");
  id++;
  perguntas.insert(id,
      "1.14. São mantidos íntegros, conservados, livres de rachaduras, trincas, vazamentos, infiltrações, bolores e descascamentos?");
  id++;
  perguntas.insert(
      id, "1.15. Possuem revestimentos lisos, impermeáveis e laváveis?");
  id++;
  perguntas.insert(id,
      "1.16. São mantidas íntegras, conservadas, livre de rachaduras, trincas, vazamentos, infiltrações, bolores e descascamentos?");
  id++;
  perguntas.insert(
      id, "1.17. Possuem revestimentos lisos, impermeáveis e laváveis?");
  id++;
  perguntas.insert(id,
      "1.18. São mantidas íntegras, conservadas, livre de rachaduras, trincas, vazamentos, infiltrações, bolores e descascamentos?");
  id++;
  perguntas.insert(id,
      "1.19. São mantidas ajustadas aos batentes (bem fechadas/vedadas) e estão bem conservadas?");
  id++;
  perguntas.insert(id,
      "1.20. Na área de preparação e armazenamento possuem fechamento automático?");
  id++;
  perguntas.insert(id,
      "1.21. Na área de preparação e armazenamento, são providas de telas milimetradas?");
  id++;
  perguntas.insert(id,
      "1.22. As janelas são mantidas ajustadas aos batentes (bem fechadas/vedadas) e estão bem conservadas?");
  id++;
  perguntas.insert(id,
      "1.23. Na área de preparação e armazenamento de alimentos, são providas de telas milimetradas?");
  id++;
  perguntas.insert(
      id, "1.24. As telas são removíveis para facilitar a limpeza periódica?");
  id++;
  perguntas.insert(id, "1.25. Quando presentes, os ralos são sifonados?");
  id++;
  perguntas.insert(id,
      "1.26. Quando presentes, as grelhas possuem dispositivo de fechamento?");
  id++;
  perguntas.insert(
      id, "1.27. Possuem dimensão compatível ao volume de resíduos?");
  id++;
  perguntas.insert(id,
      "1.28. Estão localizadas fora da área de preparação e armazenamento de alimentos?");
  id++;
  perguntas.insert(
      id, "1.29. Apresentam adequado estado de conservação e funcionamento?");
  id++;
  perguntas.insert(id,
      "1.30. A iluminação da área de preparação permite adequada visualização?");
  id++;
  perguntas.insert(id,
      "1.31. As luminárias localizadas na área de preparação são apropriadas e estão protegidas contra explosão e quedas acidentais?");
  id++;
  perguntas.insert(id,
      "1.32. Garante a renovação do ar e a manutenção do ambiente livre de fungos, gases, fumaça, pós, partículas em suspensão e condensação de vapores?");
  id++;
  perguntas.insert(id,
      "1.33. O fluxo de ar está adequado, não incidindo diretamente sobre os alimentos?");
  id++;
  perguntas.insert(id,
      "1.34. Os equipamentos e os filtros para climatização estão bem conservados?");
  id++;
  perguntas.insert(id,
      "1.35. Existe registro periódico da troca de filtros, da limpeza e da manutenção dos componentes do sistema de climatização (conforme legislação específica)?");
  id++;
  perguntas.insert(
      id, "1.36. Estão embutidas ou protegidas em tubulações externas?");
  id++;
  perguntas.insert(
      id, "1.37. São íntegras, permitindo a higienização dos ambientes?");
  id++;
  perguntas.insert(id,
      "1.38. Localizados sem comunicação direta com a área de preparação e armazenamento ou refeitórios?");
  id++;
  perguntas.insert(
      id, "1.39. Mantidos organizados e em adequado estado de conservação?");
  id++;
  perguntas.insert(
      id, "1.40. Possuem portas externas com fechamento automático?");
  id++;
  perguntas.insert(id,
      "1.41. As instalações sanitárias possuem lavatórios de mãos e os produtos destinados à higiene pessoal (papel higiênico, sabonete líquido inodoro antisséptico ou sabonete líquido inodoro e antisséptico, coletores com tampa e acionados sem contato manual e toalhas de papel não reciclado ou outro sistema higiênico e seguro para secagem das mãos)?");
  id++;
  perguntas.insert(id,
      "1.42. Existe lavatório exclusivo para a higiene das mãos na área de manipulação, em posições estratégicas em relação ao fluxo de preparo?");
  id++;
  perguntas.insert(id,
      "1.43. Existem lavatórios em número suficiente de modo a atender toda a área de preparação?");
  id++;
  perguntas.insert(id,
      "1.44. Possuem: sabonete líquido inodoro antisséptico ou sabonete inodoro e produto antisséptico, toalhas de papel não reciclado ou outro sistema higiênico e seguro de secagem das mãos e coletor de papel, acionado sem contato manual?");
  id++;
  perguntas.insert(id,
      "1.45. Quando entram em contato com alimentos, são de materiais que não transmitam substâncias tóxicas, odores, nem sabores aos alimentos (conforme legislação específica)?");
  id++;
  perguntas.insert(id,
      "1.46. São mantidos em adequado estado de conservação e são resistentes à corrosão e a repetidas operações de higienização?");
  id++;
  perguntas.insert(id,
      "1.47. Possuem as superfícies lisas, impermeáveis, laváveis e isentas de rugosidades, frestas e outras imperfeições?");
  id++;
  perguntas.insert(id,
      "1.48. São realizadas manutenções programadas e periódicas, bem como o registro dess aoperação?");
  id++;
  perguntas.insert(id,
      "1.49. É realizada a calibração dos instrumentos de medição, bem como o registro dessa operação?");
  id++;
  perguntas.insert(id,
      "1.50. Quando entram em contato com alimentos, são de materiais que não transmitam substâncias tóxicas, odores, nem sabores aos alimentos (conforme legislação específica)?");
  id++;
  perguntas.insert(id,
      "1.51. São mantidos em adequado estado de conservação e são resistentes à corrosão e a repetidas operações de higienização?");
  id++;
  perguntas.insert(id,
      "1.52. Possuem as superfícies lisas, impermeáveis, laváveis e isentas de rugosidades, frestas e outras imperfeições?");
  id++;
  perguntas.insert(id,
      "1.53. Quando entram em contato com alimentos, são de materiais que não transmitam substâncias tóxicas, odores, nem sabores aos alimentos (conforme legislação específica)?");
  id++;
  perguntas.insert(id,
      "1.54. São mantidos em adequado estado de conservação e são resistentes à corrosão e a repetidas operações de higienização?");
  id++;
  perguntas.insert(id,
      "1.55. Possuem as superfícies lisas, impermeáveis, laváveis e isentas de rugosidades, frestas e outras imperfeições?");
  id++;
  perguntas.insert(id,
      " 2.1. As instalações, equipamentos, móveis e utensílios são mantidos em condições higiênico-sanitárias? ");
  id++;
  perguntas.insert(id, "2.2. A frequência de higienização é adequada?");
  id++;
  perguntas.insert(id,
      "2.3.  Existe registro das higienizações quando não forem de rotina?");
  id++;
  perguntas.insert(id,
      "2.4. A área de preparação é higienizada quantas vezes forem necessárias e imediatamente após o término do trabalho?");
  id++;
  perguntas.insert(id, "2.5. As caixas de gordura são periodicamente limpas?");
  id++;
  perguntas.insert(id,
      "2.6.  O descarte dos resíduos das caixas de gordura é adequado (conforme legislação específica)?");
  id++;
  perguntas.insert(id,
      "2.7. As operações de higienização são realizadas por funcionários comprovadamente capacitados?");
  id++;
  perguntas.insert(id,
      "2.8. Os funcionários responsáveis pela higienização das instalações sanitárias utilizam uniformes apropriados e diferenciados daqueles utilizados na manipulação de alimentos?");
  id++;
  perguntas.insert(id, "2.9. São regularizados pelo Ministério da Saúde?");
  id++;
  perguntas.insert(id,
      "2.10. A diluição, o tempo de contato e modo de uso/aplicação obedecem às instruções recomendadas pelo fabricante?");
  id++;
  perguntas.insert(
      id, "2.11. São identificados e guardados em local reservado?");
  id++;
  perguntas.insert(id,
      "2.12. São tomadas precauções para impedir a contaminação dos alimentos por produtos químicos?");
  id++;
  perguntas.insert(id,
      "2.13. Os produtos utilizados nas áreas de preparo e armazenamento são inodoros (sem odor)?");
  id++;
  perguntas.insert(id, "2.14. São próprios para a atividade?");
  id++;
  perguntas.insert(id,
      "2.15. Estão conservados, limpos e disponíveis em número suficiente?");
  id++;
  perguntas.insert(id, "2.16. São guardados em local reservado?");
  id++;
  perguntas.insert(id,
      "2.17. Os materiais utilizados na higienização de instalações são diferentes daqueles usados para higienização das partes dos equipamentos e utensílios que entrem em contato com o alimento?");
  id++;
  perguntas.insert(id,
      "3.1. A edificação , as instalações, os equipamentos, os móveis e os utensílios são livres de vetores e pragas urbanas?");
  id++;
  perguntas.insert(id,
      "3.2. Existe um conjunto de ações eficazes e contínuas com o objetivo de impedir a atração, o abrigo, o acesso e/ou proliferação de vetores e pragas urbanas?");
  id++;
  perguntas.insert(id,
      "3.3. O controle químico é executado por empresa especializada (conforme legislação específica)?");
  id++;
  perguntas.insert(id,
      "3.4. A empresa especializada utiliza produtos regularizados pelo Ministério da Saúde?");
  id++;
  perguntas.insert(id,
      "3.5. A empresa especializada possui procedimentos pré e pós-tratamento?");
  id++;
  perguntas.insert(id,
      "3.6. Os equipamentos e utensílios, antes de serem reutilizados, são higienizados para a remoção dos resíduos de produtos químicos?");
  id++;
  perguntas.insert(id,
      "4.1. É utilizada somente água potável para a manipulação de alimentos?");
  id++;
  perguntas.insert(id,
      "4.2. Quando é utilizada solução alternativa de abastecimento de água, a potabilidade é atestada semestralmente através de laudos laboratoriais?");
  id++;
  perguntas.insert(id,
      "4.3. O reservatório é edificado e/ou revestido de materiais que não comprometam a qualidade da água (conforme legislação específica)?");
  id++;
  perguntas.insert(id,
      "4.4. O reservatório está devidamente tampado, livre de rachaduras, vazamentos, infiltrações e descascamentos?");
  id++;
  perguntas.insert(id,
      "4.5. O reservatório está em adequado estado de higiene e conservação?");
  id++;
  perguntas.insert(id,
      "4.6. O reservatório é higienizado em intervalo máximo de seis meses, sendo mantidos registros da operação?");
  id++;
  perguntas.insert(id,
      "4.7. O gelo utilizado em alimentos é fabricado a partir de água potável e é mantido em condição higiênico-sanitária?");
  id++;
  perguntas.insert(id,
      "4.8. O vapor, quando utilizado em contato com alimentos ou com superfícies que entrem em contato com alimentos, é produzido a partir de água potável?");
  id++;
  perguntas.insert(id,
      "5.1. Os coletores são identificados, íntegros, de fácil higienização e transporte?");
  id++;
  perguntas.insert(id,
      "5.2. Os coletores estão em número e capacidade suficientes para conter os resíduos?");
  id++;
  perguntas.insert(id,
      "5.3. Os coletores das áreas de preparação e armazenamento possuem tampas acionadas sem contato manual?");
  id++;
  perguntas.insert(id,
      "5.4. Os resíduos são frequentemente retirados e estocados em local fechado e isolado da área de preparação e armazenamento?");
  id++;
  perguntas.insert(id,
      "6.1. É realizado o controle da saúde dos manipuladores e este é registrado (conforme legislação específica)?");
  id++;
  perguntas.insert(id,
      "6.2. Quando apresentarem lesões e/ou sintomas de enfermidades são afastados da preparação?");
  id++;
  perguntas.insert(id,
      "6.3. Apresentam-se com uniformes compatíveis à atividade, conservados e limpos?");
  id++;
  perguntas.insert(id,
      "6.4. Os uniformes são trocados, no mínimo diariamente e usados exclusivamente nas dependências internas do estabelecimento?");
  id++;
  perguntas.insert(id,
      "6.5. As roupas e os objetos pessoais são guardados em local específico e reservado para este fim?");
  id++;
  perguntas.insert(id,
      "6.6. Lavam cuidadosamente as mãos antes de manipular o alimento, após qualquer interrupção do serviço, após usar os sanitários e sempre que se fizer necessário?");
  id++;
  perguntas.insert(id,
      "6.7. São afixados cartazes de orientação sobre a correta antissepsia das mãos e demais hábitos de higiene, em locais de fácil visualização, inclusive nas instalações sanitárias e lavatórios?");
  id++;
  perguntas.insert(id,
      "6.8. Não fumam, falam quando desnecessário, cantam, assobiam, espirram, cospem, tossem, comem, manipulam dinheiro, falam no celular ou praticam outros atos que possam contaminar o alimento?");
  id++;
  perguntas.insert(id,
      "6.9. Usam os cabelos presos e protegidos, não sendo permitido o uso de barba ou bigode?");
  id++;
  perguntas.insert(
      id, "6.10. As unhas são mantidas curtas, limpas e sem esmalte ou base?");
  id++;
  perguntas.insert(id,
      "6.11. Durante a manipulação, são retirados todos os objetos de adorno pessoal e a maquiagem?");
  id++;
  perguntas.insert(
      id, "6.12. Os manipuladores são supervisionados periodicamente?");
  id++;
  perguntas.insert(id,
      "6.13. Os visitantes cumprem os requisitos de higiene e de saúde estabelecidos para os manipuladores?");
  id++;
  perguntas.insert(
      id, "7.1. Existem critérios para avaliação e seleção dos fornecedores?");
  id++;
  perguntas.insert(id,
      "7.2. O transporte é realizado em condições adequadas de higiene e conservação?");
  id++;
  perguntas.insert(id, "7.3. A recepção é realizada em área protegida limpa?");
  id++;
  perguntas.insert(
      id, "7.4. São submetidos à inspeção e aprovação na recepção?");
  id++;
  perguntas.insert(id,
      "7.5. Somente são recepcionados as matérias-primas e os ingredientes com embalagens íntegras?");
  id++;
  perguntas.insert(id,
      "7.6. A temperatura das matérias-primas e ingredientes é verificada na recepção e no armazenamento (quando aplicável)?");
  id++;
  perguntas.insert(id,
      "7.7. Quando reprovados na recepção, são imediatamente devolvidos ao fornecedor ou são identificados, armazenados separadamente e determinado o destino final?");
  id++;
  perguntas.insert(id, "7.8. São armazenados em local limpo e organizado?");
  id++;
  perguntas.insert(
      id, "7.9. São adequadamente acondicionados e identificados?");
  id++;
  perguntas.insert(id,
      "7.10. A utilização respeita o prazo de validade ou é observada a ordem de entrada das matériasprimas e ingredientes?");
  id++;
  perguntas.insert(id,
      "7.11. São armazenados sobre paletes, estrados e/ou prateleiras, respeitando o espaçamento mínimo necessário?");
  id++;
  perguntas.insert(id,
      "7.12. Os paletes, estrados e/ou prateleiras são de material liso, resistente, impermeável e lavável?");
  id++;
  perguntas.insert(id,
      "7.13. São adotadas medidas para evitar que as matérias-primas, os ingredientes e as embalagens contaminem o alimento preparado?");
  id++;
  perguntas.insert(id,
      "8.1. As matérias-primas, ingredientes e embalagens utilizados para preparação estão em condições higiênico-sanitárias e são adequadas às atividades (conforme legislação específica)?");
  id++;
  perguntas.insert(id,
      "8.2.  O número de funcionários, equipamentos, móveis e/ou utensílios disponíveis são compatíveis com a produção?");
  id++;
  perguntas.insert(id,
      "8.3. Evita-se o contato direto ou indireto entre alimentos crus, semi-pontos e prontos?");
  id++;
  perguntas.insert(id,
      "8.4. Os funcionários que manipulam alimentos crus realizam a higiene das mãos antes de manusear alimentos prontos?");
  id++;
  perguntas.insert(id,
      "8.5. Produtos perecíveis são expostos à temperatura ambiente somente pelo tempo mínimo necessário?");
  id++;
  perguntas.insert(id,
      "8.6. Quando as matérias-primas não forem totalmente utilizadas, são adequadamente acondicionadas e identificadas?");
  id++;
  perguntas.insert(id,
      "8.7. Limpa-se as embalagens primárias das matérias-primas antes do preparo (quando aplicável)?");
  id++;
  perguntas.insert(id,
      "8.8. O tratamento térmico assegura a qualidade higiênico-sanitária dos alimentos (no mínimo 70°C, em todo alimento ou combinações adequadas de tempo e temperatura)?");
  id++;
  perguntas.insert(id,
      "8.9. São verificados o tempo e temperatura do tratamento térmico e, quando aplicável, são avaliadas às mudanças na textura e cor na parte central do alimento?");
  id++;
  perguntas.insert(id,
      "8.10. Existem medidas que garantam que o óleo e a gordura utilizados na fritura não constituam uma fonte de contaminação química para o alimento preparado?");
  id++;
  perguntas.insert(id,
      "8.11. Óleos e gorduras são aquecidos a temperaturas não superiores a 180°C?");
  id++;
  perguntas.insert(id,
      "8.12. Óleos e gorduras são substituídos imediatamente sempre que houver alteração das características físico-químicas ou sensoriais?");
  id++;
  perguntas.insert(id,
      "8.13. Alimentos congelados são descongelados antes do tratamento térmico ou segue-se às orientações constantes na rotulagem do fabricante?");
  id++;
  perguntas.insert(id,
      "8.14. O descongelamento é feito em refrigeração à temperatura inferior a 5°C, em micro-ondas ou conforme orientações constantes na rotulagem do fabricante?");
  id++;
  perguntas.insert(id,
      "8.15. Alimentos descongelados são mantidos sob refrigeração se não forem imediatamente utilizados e não são recongelados?");
  id++;
  perguntas.insert(id,
      "8.16. Alimentos preparados são conservados em temperatura superior a 60°C no máximo por 6 horas?");
  id++;
  perguntas.insert(id,
      "8.17. Alimentos preparados conservados sob refrigeração ou congelamento são previamente submetidos ao processo de resfriamento?");
  id++;
  perguntas.insert(id,
      "8.18. No resfriamento, a temperatura do alimento preparado é reduzida de 60°C a 10°C em até 2 horas?");
  id++;
  perguntas.insert(id,
      "8.19. Após o resfriamento, o alimento preparado é conservado sob refrigeração a temperaturas inferiores a 5°C, ou congelado à temperatura igual ou inferior a -18°C?");
  id++;
  perguntas.insert(id,
      "8.20. O prazo máximo de consumo do alimento preparado e conservado sob refrigeração (temperaturas inferiores a 4°C) é de 5 dias?");
  id++;
  perguntas.insert(id,
      "8.21. Os alimentos preparados e armazenados sob refrigeração ou congelamento, são adequadamente acondicionados e identificados?");
  id++;
  perguntas.insert(id,
      "8.22. A temperatura de armazenamento do alimento preparado é regularmente monitorada e registrada?");
  id++;
  perguntas.insert(id,
      "8.23. Os alimentos consumido crus são submetidos a processo de higienização (quando aplicável)?");
  id++;
  perguntas.insert(id,
      "8.24. Mantém documentado o controle e a garantia da qualidade dos alimentos?");
  id++;
  perguntas.insert(id,
      "9.1. Alimentos preparados armazenados ou aguardando o transporte são identificados e protegidos?");
  id++;
  perguntas.insert(id,
      "9.2. O armazenamento e transporte ocorrem em condições de tempo e temperatura adequados?");
  id++;
  perguntas.insert(id,
      "9.3. A temperatura do alimento preparado é monitorada durante essas etapas?");
  id++;
  perguntas.insert(id,
      "9.4. Os meios de transporte são higienizados e estão livres de vetores e pragas urbanas?");
  id++;
  perguntas.insert(id,
      "9.5. Os meios de transporte são dotados de proteção para o alimento?");
  id++;
  perguntas.insert(id,
      "9.6. Os meios de transporte somente transportam cargas que não comprometam a qualidade do alimento preparado?");
  id++;
  perguntas.insert(id,
      "10.1. As áreas de exposição e de consumo são mantidas organizadas e em adequadas condições higiênico-sanitárias?");
  id++;
  perguntas.insert(id,
      "10.2. Os manipuladores realizam anti-sepsia das mãos e utilizam utensílios ou luvas descartáveis (quando aplicável)?");
  id++;
  perguntas.insert(id,
      "10.3. Os equipamentos, móveis e utensílios disponíveis nessas áreas estão em número suficiente e em adequado estado de conservação?");
  id++;
  perguntas.insert(id,
      "10.4. Os equipamentos de exposição de alimentos estão em adequado estado de higiene e funcionamento?");
  id++;
  perguntas.insert(id,
      "10.5. A temperatura dos equipamentos de exposição é regularmente monitorada?");
  id++;
  perguntas.insert(id,
      "10.6. Os equipamentos possuem barreiras de proteção que previnam a contaminação pelo consumidor ou outras fontes?");
  id++;
  perguntas.insert(id,
      "10.7. Os utensílios utilizados na consumação são descartáveis ou são devidamente higienizados e armazenados em local protegido?");
  id++;
  perguntas.insert(id,
      "10.8. Os ornamentos e plantas da área de consumação não constituem fonte de contaminação?");
  id++;
  perguntas.insert(id,
      "10.9. A área de recebimento de dinheiro, cartões e outros meios utilizados para o pagamento de despesas, é reservada?");
  id++;
  perguntas.insert(id,
      "10.10. Os funcionários responsáveis pelos pagamentos são orientados a não manipular alimentos preparados, embalados ou não?");
  id++;
  perguntas.insert(id,
      "11.1. Dispõem de Manual de Boas Práticas e de Procedimentos Operacionais Padronizados?");
  id++;
  perguntas.insert(id,
      "11.2. Esses documentos estão acessíveis aos funcionários e à autoridade sanitária, quando requerido?");
  id++;
  perguntas.insert(id,
      "11.3. Os registros são mantidos por no mínimo 30 dias contados a partir da data de preparação dos alimentos?");
  id++;
  perguntas.insert(id,
      "11.4. Está implementado, ou seja, os procedimentos descritos estão sendo cumpridos?");
  id++;
  perguntas.insert(id,
      "11.5. Contém as instruções sequenciais das operações e a frequência de execução, especificando o nome, o cargo e/ou a função dos responsáveis pelas atividades?");
  id++;
  perguntas.insert(id,
      "11.6. Contém as seguintes informações: natureza da superfície, método, princípio ativo e concentração, tempo de contato, temperatura e desmonte dos equipamentos (quando aplicável)?");
  id++;
  perguntas.insert(
      id, "11.7. São aprovados, datados e assinados pelo responsável?");
  id++;
  perguntas.insert(id,
      "11.8. Está implementado, ou seja, os procedimentos descritos estão sendo cumpridos?");
  id++;
  perguntas.insert(id,
      "11.9. Contém as instruções sequenciais das operações e a frequência da execução, especificando o nome, o cargo e/ou a função dos responsáveis pelas atividades?");
  id++;
  perguntas.insert(id,
      "11.10. Contempla as medidas preventivas e corretivas para impedir a atração, o abrigo, o acesso e/ou a proliferação de vetores e pragas urbanas?");
  id++;
  perguntas.insert(id,
      "11.11. Quando utilizado controle químico apresenta comprovante da execução do serviço fornecido pela empresa especializada contratada (conforme legislação específica)?");
  id++;
  perguntas.insert(
      id, "11.12. São aprovados, datados e assinados pelo responsável?");
  id++;
  perguntas.insert(id,
      "11.13. Está implementado, ou seja, os procedimentos descritos estão sendo cumpridos?");
  id++;
  perguntas.insert(id,
      "11.14. Contém as instruções sequenciais das operações e a frequência da execução, especificando o nome, o cargo e/ou a função dos responsáveis pelas atividades?");
  id++;
  perguntas.insert(id,
      "11.15. Contém as seguintes informações: natureza da superfície, método, princípio ativo e concentração, tempo de contato, temperatura e desmonte dos equipamentos (quando aplicável)?");
  id++;
  perguntas.insert(id,
      "11.16. Quando realizada por empresa terceirizada apresenta o certificado de execução do serviço?");
  id++;
  perguntas.insert(
      id, "11.17. São aprovados, datados e assinados pelo responsável?");
  id++;
  perguntas.insert(id,
      "11.18. Está implementado, ou seja, os procedimentos descritos estão sendo cumpridos?");
  id++;
  perguntas.insert(id,
      "11.19. Contém as instruções sequenciais das operações e a frequência da execução, especificando o nome, o cargo e/ou a função dos responsáveis pelas atividades?");
  id++;
  perguntas.insert(id,
      "11.20. Contempla as etapas, a frequência e os princípios ativos usados na antissepsia das mãos dos manipuladores?");
  id++;
  perguntas.insert(id,
      "11.21. Contempla as medidas adotadas em caso de lesão nas mãos, sintomas de enfermidade ou suspeita de problema de saúde?");
  id++;
  perguntas.insert(id,
      "11.22. Especifica os exames que os manipuladores são submetidos, bem como a periodicidade de sua execução?");
  id++;
  perguntas.insert(id,
      "11.23. Descreve o programa de capacitação em higiene, com a carga horária, o conteúdo programático e a frequência de realização e a forma de arquivar os registros?");
  id++;
  perguntas.insert(
      id, "11.24. São aprovados, datados e assinados pelo responsável?");
  id++;
  perguntas.insert(id,
      "12.1. São capacitados periodicamente em: higiene pessoal, contaminantes alimentares, doenças transmitidas por alimentos, manipulação higiênica dos alimentos e boas práticas?");
  id++;
  perguntas.insert(
      id, "12.2. A capacitação é comprovada mediante documentação?");
  id++;

  //print("${perguntas.length}");
}

void inicializarSubtitulos() {
  int id = 0;
  subtitulos.insert(id, "Áreas Externas");
  id++;
  subtitulos.insert(id, "Áreas Internas");
  id++;
  subtitulos.insert(id, "Edificações e Instalações");
  id++;
  subtitulos.insert(id, "Instalações Físicas - Pisos");
  id++;
  subtitulos.insert(id, "Instalações Físicas - Paredes");
  id++;
  subtitulos.insert(id, "Instalações Físicas - Tetos/Forros");
  id++;
  subtitulos.insert(id, "Portas");
  id++;
  subtitulos.insert(id, "Janelas e Outras Aberturas (sistema de exaustão)");
  id++;
  subtitulos.insert(id, "Ralos e Grelhas");
  id++;
  subtitulos.insert(id, "Caixa de Gordura e Esgoto");
  id++;
  subtitulos.insert(id, "Iluminação");
  id++;
  subtitulos.insert(id, "Ventilação");
  id++;
  subtitulos.insert(id, "Instalações Elétricas");
  id++;
  subtitulos.insert(id, "Instalações Sanitárias");
  id++;
  subtitulos.insert(id, "Lavatório Área de Manipulação");
  id++;
  subtitulos.insert(id, "Equipamentos");
  id++;
  subtitulos.insert(id, "Utensílios");
  id++;
  subtitulos.insert(id, "Móveis");
  id++;
  subtitulos.insert(
      id, "Produtos Saneantes (higiene ambiental, pessoal e do alimento)");
  id++;
  subtitulos.insert(id, "Utensílios e Equipamentos utilizados na Higienização");
  id++;
  subtitulos.insert(
      id, "POP Higienização de Instalações, Equipamentos e Móveis");
  id++;
  subtitulos.insert(id, "POP Controle Integrado de Vetores e  Pragas Urbanas");
  id++;
  subtitulos.insert(id, "POP Higienização do Reservatório");
  id++;
  subtitulos.insert(id, "POP Higiene e Saúde dos Manipuladores ");
  id++;
  //print("${subtitulos.length}");
}

