import 'package:apetite_project/primeiraPagina.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:apetite_project/recursos.dart';

String _nomeEmp = "valor original";
String _nomeAvaliadora = " valor original";

String getNome() => _nomeAvaliadora;
String getNomeEmp() => _nomeEmp;

void main() {
  runApp(MaterialApp(
    home: TelaInicial(),
    theme: ThemeData(hintColor: Colors.white),
  ));
}

class TelaInicial extends StatefulWidget {
  @override
  _TelaInicialState createState() => _TelaInicialState();
}

class _TelaInicialState extends State<TelaInicial> {
  @override
  Widget build(BuildContext context) {
    _portraitModeOnly();
    SystemChrome.setEnabledSystemUIOverlays([]);
    return Scaffold(
      backgroundColor: Colors.transparent,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButtonAnimator: FloatingActionButtonAnimator.scaling,
      floatingActionButton: FloatingActionButton(
        isExtended: true,
        backgroundColor: Colors.white,
        child: Text(
          "Iniciar Avaliação",
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 9,
            color: Colors.black,
          ),
        ),
        onPressed: () {
          print("Botão apertado");
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => Home()));
        },
      ),
      body: Stack(
        children: <Widget>[
          Image.asset(
            "images/fundocomlogo.jpg",
            fit: BoxFit.cover,
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
          ),
        ],
      ),
    );
  }
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  TextEditingController nomeController = TextEditingController();
  TextEditingController nomeEmpController = TextEditingController();

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  void _cadastrarNomes() {
    setState(() {
      print("$_nomeEmp");
      print("$_nomeAvaliadora");
    });
  }

  @override
  void initState() {
    super.initState();
    inicializar();
    //print("Entrou no initState");
  }

  static void inicializar() {
    for (int i = 0; i < 182; i++) {
      respNao.insert(i, false);
      respAd.insert(i, false);
      respIna.insert(i, false);
      respostas.insert(i, "");
      ina.insert(i, "");
      inadequacoes.insert(i, null);
      visible.insert(i, false);
      perguntas.insert(i, "");
    }
    inicializarPerguntas();
    inicializarSubtitulos();
  }

  @override
  Widget build(BuildContext context) {
    _portraitModeOnly();
    SystemChrome.restoreSystemUIOverlays();
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          SingleChildScrollView(
            child: Stack(
              children: <Widget>[
                Image.asset(
                  "images/fundocomlogo.jpg",
                  fit: BoxFit.cover,
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        Center(
                          child: Padding(
                            padding: EdgeInsets.only(
                              top: 300.0,
                            ),
                            child: TextFormField(
                                autofocus: true,
                                cursorColor: Colors.white70,
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                  focusColor: Colors.redAccent,
                                  labelText: "Avaliador(a)",
                                  hintText: "Digite o nome do(a) Avaliador(a)",
                                  alignLabelWithHint: true,
                                  hintStyle: TextStyle(fontSize: 20.0),
                                  labelStyle: TextStyle(color: Colors.white),
                                  icon: Icon(Icons.person_outline, size: 25),
                                ),
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.white, fontSize: 25.0),
                                controller: nomeController,
                                validator: (value) {
                                  if (value.isEmpty)
                                    return "Este campo não pode ficar vazio.";
                                  else
                                    _nomeAvaliadora = value;
                                }),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 5.0),
                          child: TextFormField(
                              cursorColor: Colors.white70,
                              keyboardType: TextInputType.text,
                              decoration: InputDecoration(
                                focusColor: Colors.redAccent,
                                labelText: "Estabelecimento",
                                hintText: "Digite o nome do Estabelecimento",
                                hintStyle: TextStyle(fontSize: 20.0),
                                alignLabelWithHint: true,
                                labelStyle: TextStyle(color: Colors.white),
                                icon: Icon(Icons.store, size: 25),
                              ),
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.white, fontSize: 25.0),
                              controller: nomeEmpController,
                              validator: (value) {
                                if (value.isEmpty)
                                  return "Este campo não pode ficar vazio.";
                                else
                                  _nomeEmp = value;
                              }),
                        ),
                        Padding(
                          padding:
                              EdgeInsets.only(top: 90.0, right: 20.0, left: 20),
                          child: Card(
                            color: Colors.transparent,
                            elevation: 10.0,
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(50.0)),
                              onPressed: () {
                                if (_formKey.currentState.validate()) {
                                  setState(
                                    () {
                                      _cadastrarNomes();
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  PrimeiraPagina()));
                                    },
                                  );
                                }
                              },
                              child: Text("Iniciar Avaliação",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 23.0,
                                      fontWeight: FontWeight.bold)),
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

void _portraitModeOnly() {
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
}

void _enableRotation() {
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
    DeviceOrientation.landscapeLeft,
    DeviceOrientation.landscapeRight,
  ]);
}
