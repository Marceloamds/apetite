import 'package:apetite_project/decimaSegundaPagina.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:apetite_project/recursos.dart';

class DecimaPrimeiraPagina extends StatefulWidget {
  @override
  _DecimaPrimeiraPagina createState() => _DecimaPrimeiraPagina();
}

class _DecimaPrimeiraPagina extends State<DecimaPrimeiraPagina> {
  static GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "11. DOCUMENTAÇÃO E REGISTRO",
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
        ),
        elevation: 7.0,
        backgroundColor: Colors.red[900],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButtonAnimator: FloatingActionButtonAnimator.scaling,
      floatingActionButton: FloatingActionButton(
        isExtended: true,
        backgroundColor: Colors.white,
        child: Icon(
          Icons.arrow_forward,
          color: Colors.redAccent,
        ),
        onPressed: () {
          if (_formKey.currentState.validate()) {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => DecimaSegundaPagina()));
          }
        },
      ),
      body: conteudo(),
    );
  }

  Widget conteudo() {
    return Builder(
      builder: (BuildContext context) => ListView(
        padding: EdgeInsets.only(left: 10.0, right: 10.0),
        children: <Widget>[
          Form(
            key: _formKey,
            child: Padding(
              padding: EdgeInsets.all(10),
              child: Column(
                children: <Widget>[
                  BuildPerguntas(id: 156),
                  BuildPerguntas(id: 157),
                  BuildPerguntas(id: 158),
                  BuildPerguntas(id: 159),
                  buildSubtitulos(19),
                  BuildPerguntas(id: 160),
                  BuildPerguntas(id: 161),
                  BuildPerguntas(id: 162),
                  BuildPerguntas(id: 163),
                  buildSubtitulos(20),
                  BuildPerguntas(id: 164),
                  BuildPerguntas(id: 165),
                  BuildPerguntas(id: 166),
                  BuildPerguntas(id: 167),
                  BuildPerguntas(id: 168),
                  buildSubtitulos(21),
                  BuildPerguntas(id: 169),
                  BuildPerguntas(id: 170),
                  BuildPerguntas(id: 171),
                  BuildPerguntas(id: 172),
                  BuildPerguntas(id: 173),
                  buildSubtitulos(22),
                  BuildPerguntas(id: 174),
                  BuildPerguntas(id: 175),
                  BuildPerguntas(id: 176),
                  BuildPerguntas(id: 177),
                  BuildPerguntas(id: 178),
                  BuildPerguntas(id: 179),
                 
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
