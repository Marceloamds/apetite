import 'package:apetite_project/terceiraPagina.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:apetite_project/recursos.dart';

class SegundaPagina extends StatefulWidget {
  @override
  _SegundaPagina createState() => _SegundaPagina();
}

class _SegundaPagina extends State<SegundaPagina> {
  static GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "2. HIGIENIZAÇÃO INSTALAÇÕES,\nEQUIPAMENTOS, MÓVEIS E UTENSÍLIOS",
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold),
        ),
        elevation: 7.0,
        backgroundColor: Colors.red[900],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButtonAnimator: FloatingActionButtonAnimator.scaling,
      floatingActionButton: FloatingActionButton(
        isExtended: true,
        backgroundColor: Colors.white,
        child: Icon(
          Icons.arrow_forward,
          color: Colors.redAccent
          /*color: colorSeta[1],*/
        ),
        onPressed: () {
          if (_formKey.currentState.validate()) {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => TerceiraPagina()));
          }
        },
      ),
      body: conteudo(),
    );
  } 
  Widget conteudo(){
    return Builder(
        builder: (BuildContext context) => ListView(
          padding: EdgeInsets.only(left: 10.0, right: 10.0),
          children: <Widget>[
            Form(
              key: _formKey,
              child: Padding(
                padding: EdgeInsets.all(10),
                child: Column(
                  children: <Widget>[
                    BuildPerguntas(id: 55),
                    BuildPerguntas(id: 56),
                    BuildPerguntas(id: 57),
                    BuildPerguntas(id: 58),
                    BuildPerguntas(id: 59),
                    BuildPerguntas(id: 60),
                    BuildPerguntas(id: 61),
                    BuildPerguntas(id: 62),
                    buildSubtitulos(17),
                    BuildPerguntas(id: 63),
                    BuildPerguntas(id: 64),
                    BuildPerguntas(id: 65),
                    BuildPerguntas(id: 66),
                    BuildPerguntas(id: 67),
                    buildSubtitulos(18),
                    BuildPerguntas(id: 68),
                    BuildPerguntas(id: 69),
                    BuildPerguntas(id: 70),
                    BuildPerguntas(id: 71),
                  ],
                ),
              ),
            ),
          ],
        ),
      );
  }
}
